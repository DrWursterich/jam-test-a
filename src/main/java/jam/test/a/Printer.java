package jam.test.a;

import java.io.PrintStream;

public class Printer {

	private PrintStream printStream;

	public Printer(final PrintStream printStream) {
		this.printStream = printStream;
	}

	public void print(final String message) {
		this.printStream.println(message);
	}
}
